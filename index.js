"use strict";

var request = require('request');
var CliqueUsersApi = require('./apis/users');
var CliqueInviteApi = require('./apis/invite');
var CliqueContextsApi = require('./apis/contexts');
var CliqueConferencesApi = require('./apis/conferences');
var CliqueRecordingApi = require('./apis/recording');
var CliqueChatApi = require('./apis/chat');
var CliqueWhitelistsApi = require('./apis/whitelists');

class CliqueApi {
    constructor(apiKey, apiUrl) {
        if (!apiKey) throw new Error('API key should be provided');

        this.apiKey = apiKey;
        this.apiUrl = apiUrl || 'https://caw.me/api/v2/';

        this.userApi = new CliqueUsersApi(this);
        this.inviteApi = new CliqueInviteApi(this);
        this.conferencesApi = new CliqueConferencesApi(this);
        this.contextsApi = new CliqueContextsApi(this);
        this.recordingApi = new CliqueRecordingApi(this);
        this.chatApi = new CliqueChatApi(this);
        this.whitelistsApi = new CliqueWhitelistsApi(this);
    }

    getNextRequestApiKey() {
        return this.apiKey;
    }

    /* this methods is here for backward compatibility */
    newUser(userData) {
        return this.userApi.createUser(userData);
    }

    getUserById(userId) {
        return this.userApi.getUserById(userId);
    }

    updateUser(userData) {
        return this.userApi.updateUser(userData);
    }

    userToken(user_id) {
        return this.userApi.getUserToken(user_id);
    }

    createNewConference(confData) {
        return this.conferencesApi.createConference(confData);
    }

    getConferenceById(confId) {
        return this.conferencesApi.getConferenceById(confId);
    }

    getUserListHash(confId) {
        return this.conferencesApi.getConferenceParticipantsList(confId);
    }
}

var client;

module.exports = function(apiKey, apiUrl) {
    if (!(client instanceof CliqueApi)) {
        client = new CliqueApi(apiKey, apiUrl);
    }

    return client;
}

'use strict';

var CliqueApiBase = require('./apiBaseClass');

class CliqueRecordingApi extends CliqueApiBase {
    startRecording(conf_id) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error('Conf id is required'));

            this._sendPostRequest(`/recording/${conf_id}/start`, {}, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.record);
            });
        });
    }

    stopRecording(conf_id) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error('Conf id is required'));

            this._sendPostRequest(`/recording/${conf_id}/stop`, {}, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.record);
            });
        });
    }

    conferenceRecording(conf_id) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error('Conf id is required'));

            this._sendGetRequest(`/recording/${conf_id}/conference`, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.record);
            });
        });
    }

    getRecording(record_uid) {
        return new Promise((resolve, reject) => {
            if (!record_uid) return reject(new Error('Recording uid is required'));

            this._sendGetRequest(`/recording/${record_uid}`, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.record);
            });
        });
    }

    deleteRecording(record_uid) {
        return new Promise((resolve, reject) => {
            if (!record_uid) return reject(new Error('Recording uid is required'));

            this._sendDeleteRequest(`/recording/${record_uid}`, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.record);
            });
        });
    }
}

module.exports = CliqueRecordingApi;

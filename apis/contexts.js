"use strict";

var CliqueApiBase = require('./apiBaseClass');
const querystring = require('querystring');

class CliqueContextsApi extends CliqueApiBase {
    createContext(contextData) {
      return new Promise((resolve, reject) => {
          this._sendPostRequest('/contexts', contextData, (err, res, body) => {
              if (err) return reject(err);
              if (!body.ok) return reject(body.error);

              return resolve(body.context);
          });
      });
    }

    getContextById(context_id) {
      return new Promise((resolve, reject) => {
          if (!context_id) return reject(new Error("Context id is required"));

          this._sendGetRequest(`/contexts/${context_id}`, (err, res, body) => {
            if (err) return reject(err);
            if (body.ok) return resolve(body.context);

            reject(body.error);
          });
      });
    }

    updateContext(contextData) {
        return new Promise((resolve, reject) => {
            if (!contextData || !contextData.id) return reject(new Error("Invalid context data passed."));

            this._sendPutRequest(`/contexts/${contextData.id}`, contextData, (err, res, body) => {
                if (err) return reject(err);
                if (body.ok) return resolve(body.context);

                reject(body.error);
            });
        });
    }

    search(searchParams) {
      return new Promise((resolve, reject) => {
          if (!searchParams || !Object.keys(searchParams).length) return reject(new Error("searchParams are required"));

          this._sendGetRequest(`/contexts?${querystring.stringify(searchParams)}`, (err, res, body) => {
            if (err) return reject(err);
            if (body.ok) return resolve(body.result);

            reject(body.error);
          });
      });
    }
}

module.exports = CliqueContextsApi;

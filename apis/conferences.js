"use strict";

var CliqueApiBase = require('./apiBaseClass');
const querystring = require('querystring');

class CliqueConferencesApi extends CliqueApiBase {
    createConference(confData) {
      return new Promise((resolve, reject) => {
          this._sendPostRequest('/conferences', confData, (err, res, body) => {
              if (err) return reject(err);
              if (!body.ok) return reject(body.error);

              return resolve(body.conference);
          });
      });
    }

    getConferenceById(conf_id) {
      return new Promise((resolve, reject) => {
          if (!conf_id) return reject(new Error("Conf id is required"));

          this._sendGetRequest(`/conferences/${conf_id}`, (err, res, body) => {
            if (err) return reject(err);
            if (body.ok) return resolve(body.conference);

            reject(body.error);
          });
      });
    }

    getConferenceParticipantsList(conf_id) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error("Conf id is required"));

            this._sendGetRequest(`/conferences/${conf_id}/user-list-hash`, (err, res, body) => {
              if (err) return reject(err);
              if (body.ok) return resolve(body.result);

              reject(body.error);
            });
        });
    }

    updateConference(confData) {
      return new Promise((resolve, reject) => {
          if (!confData || !confData.id) return reject(new Error("Invalid conference data passed."));

          this._sendPutRequest(`/conferences/${confData.id}`, confData, (err, res, body) => {
              if (err) return reject(err);
              if (body.ok) return resolve(body.conference);

              reject(body.error);
          });
      });
    }

    search(searchParams) {
      return new Promise((resolve, reject) => {
          if (!searchParams || !Object.keys(searchParams).length) return reject(new Error("searchParams are required"));

          this._sendGetRequest(`/conferences?${querystring.stringify(searchParams)}`, (err, res, body) => {
            if (err) return reject(err);
            if (body.ok) return resolve(body.result);

            reject(body.error);
          });
      });
    }

    startConference(conf_id) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error("Conf id is required"));

            this._sendPostRequest(`/conferences/${conf_id}/start`, {}, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.conference);
            });
        });
    }

    assignPhoneNumber(conf_id, options) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error("Conf id is required"));

            this._sendPostRequest(`/conferences/${conf_id}/phone_number`, options || {}, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.phone_number);
            });
        });
    }

    updatePhoneNumber(conf_id, options) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error("Conf id is required"));

            this._sendPutRequest(`/conferences/${conf_id}/phone_number`, options || {}, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.phone_number);
            });
        });
    }

    releasePhoneNumber(conf_id) {
        return new Promise((resolve, reject) => {
            if (!conf_id) return reject(new Error("Conf id is required"));

            this._sendDeleteRequest(`/conferences/${conf_id}/phone_number`, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve();
            });
        });
    }
}

module.exports = CliqueConferencesApi;

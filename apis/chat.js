"use strict";

var CliqueApiBase = require('./apiBaseClass');
const querystring = require('querystring');

class CliqueChatApi extends CliqueApiBase {
    createChat(chatData) {
      return new Promise((resolve, reject) => {
          this._sendPostRequest('/chats', chatData, (err, res, body) => {
              if (err) return reject(err);
              if (!body.ok) return reject(body.error);

              return resolve(body.chat);
          });
      });
    }

    joinChat(chatId) {
      return new Promise((resolve, reject) => {
          this._sendPostRequest('/chats/' + chatId + '/join', chatData, (err, res, body) => {
              if (err) return reject(err);
              if (!body.ok) return reject(body.error);

              return resolve(body.chat);
          });
      });
    }

}

module.exports = CliqueChatApi;

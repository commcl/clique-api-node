"use strict";

var CliqueApiBase = require('./apiBaseClass');
const querystring = require('querystring');

class CliqueWhitelistsApi extends CliqueApiBase {

    createWhitelist(conference_id, allowed_users) {
      return new Promise((resolve, reject) => {
          this._sendPostRequest('/whitelists', {conference_id: conference_id, allowed_users: allowed_users}, (err, res, body) => {
              if (err) return reject(err);
              if (!body.ok) return reject(body.error);

              return resolve(body.whitelist);
          });
      });
    }

    getWhitelist(conference_id) {
        return new Promise((resolve, reject) => {
            if (!conference_id) return reject(new Error("conference_id is required"));

            this._sendGetRequest(`/whitelists/${conference_id}`, (err, res, body) => {
              if (err) return reject(err);
              if (body.ok) return resolve(body.whitelist);

              reject(body.error);
            });
        });
    }

    addUsersToWhitelist(conference_id, user_uuids) {
        return new Promise((resolve, reject) => {
            if (!conference_id || !user_uuids || !user_uuids.length) return reject(new Error("Invalid method data passed."));

            this._sendPutRequest(`/whitelists/${conference_id}/add_users`, {users: user_uuids}, (err, res, body) => {
                if (err) return reject(err);
                if (body.ok) return resolve(body.whitelist);

                reject(body.error);
            });
        });
    }

    delUsersFromWhitelist(conference_id, user_uuids) {
        return new Promise((resolve, reject) => {
            if (!conference_id || !user_uuids || !user_uuids.length) return reject(new Error("Invalid method data passed."));

            this._sendPutRequest(`/whitelists/${conference_id}/del_users`, {users: user_uuids}, (err, res, body) => {
                if (err) return reject(err);
                if (body.ok) return resolve(body.whitelist);

                reject(body.error);
            });
        });
    }

    deleteWhitelist(conference_id) {
        return new Promise((resolve, reject) => {
            if (!conference_id) return reject(new Error("conference_id is required"));

            this._sendDeleteRequest(`/whitelists/${conference_id}`, (err, res, body) => {
              if (err) return reject(err);
              if (body.ok) return resolve();

              reject(body.error);
            });
        });
    }
}

module.exports = CliqueWhitelistsApi;

"use strict";

var CliqueApiBase = require('./apiBaseClass');

class CliqueInviteApi extends CliqueApiBase {
    createInviteTemplate(templateData) {
      return new Promise((resolve, reject) => {
          this._sendPostRequest('/invites/templates', templateData, (err, res, body) => {
              if (err) return reject(err);
              if (!body.ok) return reject(body.error);

              return resolve(body.template);
          });
      });
    }

    getInviteTemplateByName(template_name) {
      return new Promise((resolve, reject) => {
          if (!template_name) return reject(new Error("Template name is required"));

          this._sendGetRequest(`/invites/templates/${template_name}`, (err, res, body) => {
            if (err) return reject(err);
            if (body.ok) return resolve(body.template);

            reject(body.error);
          });
      });
    }

    updateInviteTemplate(templateData) {
      return new Promise((resolve, reject) => {
          if (!templateData || !templateData.name) return reject(new Error("Invalid template data passed."));

          this._sendPutRequest(`/invites/templates/${templateData.name}`, templateData, (err, res, body) => {
              if (err) return reject(err);
              if (body.ok) return resolve(body.template);

              reject(body.error);
          });
      });
    }

    deleteInviteTemplate(template_name) {
        return new Promise((resolve, reject) => {
            if (!template_name) return reject(new Error("Template name is required"));

            this._sendDeleteRequest(`/invites/templates/${template_name}`, (err, res, body) => {
              if (err) return reject(err);
              if (body.ok) return resolve(true);

              reject(body.error);
            });
        });
    }

    sendInvites(conference_id, invitesData) {
        if (!conference_id || !invitesData ) return reject(new Error("Invalid data passed."));
        return new Promise((resolve, reject) => {
            this._sendPostRequest(`/invites/${conference_id}/send`, invitesData, (err, res, body) => {
                if (err) return reject(err);
                if (!body.ok) return reject(body.error);

                return resolve(body.failures);
            });
        });
    }
}

module.exports = CliqueInviteApi;

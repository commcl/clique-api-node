"use strict";

var request = require('request');

class CliqueApiBase {
  constructor(wrapper) {
    this.wrapper = wrapper;
    this.request = request;
  }

  _sendPostRequest(method, body, cb) {
    request.post({
        url: this.wrapper.apiUrl + method,
        body: body,
        json: true,
        auth: {
            bearer: this.wrapper.getNextRequestApiKey()
        }
    }, cb);
  }

  _sendGetRequest(path, cb) {
    request.get({
        url: this.wrapper.apiUrl + path,
        json: true,
        auth: {
            bearer: this.wrapper.getNextRequestApiKey()
        }
    }, cb);
  }

  _sendPutRequest(method, body, cb) {
    request.put({
        url: this.wrapper.apiUrl + method,
        body: body,
        json: true,
        auth: {
            bearer: this.wrapper.getNextRequestApiKey()
        }
    }, cb);
  }

  _sendDeleteRequest(path, cb) {
      request.delete({
          url: this.wrapper.apiUrl + path,
          json: true,
          auth: {
              bearer: this.wrapper.getNextRequestApiKey()
          }
      }, cb);
  }
}

module.exports = CliqueApiBase;

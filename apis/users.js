"use strict";

var CliqueApiBase = require('./apiBaseClass');
const querystring = require('querystring');

class CliqueUsersApi extends CliqueApiBase {
  createUser(userData) {
    return new Promise((resolve, reject) => {
        this._sendPostRequest('/users', userData, (err, res, body) => {
            if (err) return reject(err);
            if (!body.ok) return reject(body.error);

            return resolve(body.user);
        });
    });
  }

  getUserById(user_id) {
    return new Promise((resolve, reject) => {
        if (!user_id) return reject(new Error("User uuid is required"));

        this._sendGetRequest(`/users/${user_id}`, (err, res, body) => {
          if (err) return reject(err);

          if (body.ok) return resolve(body.user);

          reject(body.error);
        });
    });
  }

  updateUser(userData) {
    return new Promise((resolve, reject) => {
        if (!userData || !userData.uuid) return reject(new Error("User uuid is not set"));

        this._sendPutRequest(`/users/${userData.uuid}`, userData, (err, res, body) => {
            if (err) return reject(err);

            if (body.ok) return resolve(body.user);

            reject(body.error);
        });
    });
  }

  getUserToken(user_id) {
    return new Promise((resolve, reject) => {
        if (!user_id) return reject(new Error("User uuid is required"));

        this._sendGetRequest(`/token/${user_id}`, (err, res, body) => {
          if (err) return reject(err);

          if (body.ok) return resolve(body.token);

          reject(body.error);
        });
    });
  }

  getCallHistory(user_id, top, skip) {
    return new Promise((resolve, reject) => {
        if (!user_id) return reject(new Error("User uuid is required"));

        this._sendGetRequest(`/users/${user_id}/call_history?top=${top || 10}&skip=${skip || 0}`, (err, res, body) => {
          if (err) return reject(err);

          if (body.ok) return resolve(body.history);

          reject(body.error);
        });
    });
  }

  search(searchParams) {
    return new Promise((resolve, reject) => {
        if (!searchParams || !Object.keys(searchParams).length) return reject(new Error("searchParams are required"));

        this._sendGetRequest(`/users?${querystring.stringify(searchParams)}`, (err, res, body) => {
          if (err) return reject(err);
          if (body.ok) return resolve(body.result);

          reject(body.error);
        });
    });
  }
}

module.exports = CliqueUsersApi;
